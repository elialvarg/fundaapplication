﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using BusinessLogicLayer;
using BusinessLogicLayer.Interfaces;
using BusinessLogicLayer.Models;
using Common;
using DataAccessLayer.Interfaces;
using Moq;
using NUnit.Framework;
using NUnit.Framework.Internal;


namespace BusinessLogicLayerTests
{
    [TestFixture]
    public class AppLogicTest
    {
        private const string UrlMakelaarAmsterdam = "http://partnerapi.funda.nl/feeds/Aanbod.svc/JSON/ac1b0b1572524640a0ecc54de453ea9f/?type=koop&zo=/amsterdam";
        private const string UrlMakelaarAmsterdamWithTuin = "http://partnerapi.funda.nl/feeds/Aanbod.svc/JSON/ac1b0b1572524640a0ecc54de453ea9f/?type=koop&zo=/amsterdam/tuin";

        private const string DataPath = "..\\..\\..\\Data\\";
        private readonly string _JsonfileLocation = AppDomain.CurrentDomain.BaseDirectory + DataPath;

        private IAppLogic _appLogic;
        private Mock<IFundaEntityRepository> _mockFundaEntityRepository;
        private Mock<IFundaApiProxy> _mockFundaApiProxy;
        private Mock<IConsole> _mockConsole;
        private Mock<IFundaDtoToEntityConverter> _mockFundaDtoToEntityConverter;
        private Mock<ILoggerWrapper> _mockLoggerWrapper;

        [SetUp]
        public void SetUp()
        {
            _mockFundaEntityRepository = new Mock<IFundaEntityRepository>();
            _mockFundaApiProxy = new Mock<IFundaApiProxy>();
            _mockConsole = new Mock<IConsole>();
            _mockFundaDtoToEntityConverter = new Mock<IFundaDtoToEntityConverter>();
            _mockLoggerWrapper = new Mock<ILoggerWrapper>();
            _appLogic = new AppLogic(_mockFundaEntityRepository.Object,
                                    _mockFundaApiProxy.Object,
                                    _mockConsole.Object,
                                    _mockFundaDtoToEntityConverter.Object,
                                    _mockLoggerWrapper.Object);
        }

        [TearDown]
        public void TearDown()
        {
            _mockFundaEntityRepository = null;
            _mockFundaApiProxy = null;
            _mockConsole = null;
            _mockFundaDtoToEntityConverter = null;
            _mockLoggerWrapper = null;
            _appLogic = null;
        }

        [Test]
        public void GivenASystemRunning_WhenCallingRunWithOptionOne_ThenGetTopAmsterdamMakelaarsIsCalled()
        {
            // Arrange
            FundaDto fundaDto = new FundaDto
            {
                Paging = new Paging {AantalPaginas = 1},
                Items = new List<FundaObjectDto>
                {
                    new FundaObjectDto {MakelaarNaam = "M1"},
                    new FundaObjectDto {MakelaarNaam = "M1"},
                    new FundaObjectDto {MakelaarNaam = "M2"},
                    new FundaObjectDto {MakelaarNaam = "M3"},
                    new FundaObjectDto {MakelaarNaam = "M4"},
                    new FundaObjectDto {MakelaarNaam = "M4"}
                }
            };

            List<FundaEntity> fundaEntities = new List<FundaEntity>
            {
                new FundaEntity {MakelaarName = "M1", TotalObjects = 2},
                new FundaEntity {MakelaarName = "M2", TotalObjects = 1},
                new FundaEntity {MakelaarName = "M3", TotalObjects = 1},
                new FundaEntity {MakelaarName = "M4", TotalObjects = 2},
            };

            _mockConsole.SetupSequence(m => m.ReadLine()).Returns("1").Returns("exit");
            _mockFundaApiProxy.Setup(x => x.GetFundaObjectDto(UrlMakelaarAmsterdam)).Returns(Task.FromResult(fundaDto));
            _mockFundaDtoToEntityConverter.Setup(x => x.ToFundaEntity(fundaDto.Items)).Returns(fundaEntities);
            _mockFundaEntityRepository.Setup(x => x.GetTopAmsterdamMakelaars(10)).Returns(fundaEntities);

            // Act
            _appLogic.Run();

            // Assert
            _mockFundaApiProxy.Verify(m => m.GetFundaObjectDto(UrlMakelaarAmsterdam), Times.Once);
            _mockFundaDtoToEntityConverter.Verify(m => m.ToFundaEntity(It.IsAny<List<FundaObjectDto>>()), Times.Once);
            _mockFundaEntityRepository.Verify(m => m.GetTopAmsterdamMakelaars(10), Times.Once);
        }
    }
}
