﻿namespace BusinessLogicLayer
{
    public interface IAppLogic
    {
        void Run();
    }
}