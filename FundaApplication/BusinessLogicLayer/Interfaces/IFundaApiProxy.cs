﻿using System.Threading;
using System.Threading.Tasks;
using BusinessLogicLayer.Models;

namespace BusinessLogicLayer.Interfaces
{
    /// <summary>
    /// Defines the available resources that can be accessed using FundaApiProxy.
    /// </summary>
    public interface IFundaApiProxy
    {
        Task<FundaDto> GetFundaObjectDto(string url);
    }
}