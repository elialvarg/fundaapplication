﻿using System.Collections.Generic;
using BusinessLogicLayer.Models;
using DataAccessLayer.Interfaces;

namespace BusinessLogicLayer.Interfaces
{
    public interface IFundaDtoToEntityConverter
    {
        /// <summary>
        /// Converts from FundaObjectDto to FundaEntity.
        /// </summary>
        IList<FundaEntity> ToFundaEntity(IList<FundaObjectDto> dto);
    }
}