﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace BusinessLogicLayer.Models
{
    /// <summary>
    /// Data Contract used for deserializing JSON coming from FundaApi.
    /// </summary>
    [DataContract]
    public class FundaDto
    {
        [DataMember(Name = "Objects")]
        public List<FundaObjectDto> Items { get; set; }

        [DataMember(Name = "Paging")]
        public Paging Paging { get; set; }

        [DataMember(Name = "TotaalAantalObjecten")]
        public long TotaalAantalObjecten { get; set; }
    }
    
    [DataContract]
    public class FundaObjectDto
    {
        [DataMember]
        public string AangebodenSindsTekst { get; set; }

        [DataMember]
        public string AanmeldDatum { get; set; }

        [DataMember]
        public object AantalBeschikbaar { get; set; }

        [DataMember]
        public long AantalKamers { get; set; }

        [DataMember]
        public object AantalKavels { get; set; }

        [DataMember]
        public string Aanvaarding { get; set; }

        [DataMember]
        public string Adres { get; set; }

        [DataMember]
        public long Afstand { get; set; }

        [DataMember]
        public string BronCode { get; set; }

        [DataMember]
        public object[] ChildrenObjects { get; set; }

        [DataMember]
        public object DatumAanvaarding { get; set; }

        [DataMember]
        public object DatumOndertekeningAkte { get; set; }

        [DataMember]
        public Uri Foto { get; set; }

        [DataMember]
        public Uri FotoLarge { get; set; }

        [DataMember]
        public Uri FotoLargest { get; set; }

        [DataMember]
        public Uri FotoMedium { get; set; }

        [DataMember]
        public Uri FotoSecure { get; set; }

        [DataMember]
        public object GewijzigdDatum { get; set; }

        [DataMember]
        public long GlobalId { get; set; }

        [DataMember]
        public Guid GroupByObjectType { get; set; }

        [DataMember]
        public bool Heeft360GradenFoto { get; set; }

        [DataMember]
        public bool HeeftBrochure { get; set; }

        [DataMember]
        public bool HeeftOpenhuizenTopper { get; set; }

        [DataMember]
        public bool HeeftOverbruggingsgrarantie { get; set; }

        [DataMember]
        public bool HeeftPlattegrond { get; set; }

        [DataMember]
        public bool HeeftTophuis { get; set; }

        [DataMember]
        public bool HeeftVeiling { get; set; }

        [DataMember]
        public bool HeeftVideo { get; set; }

        [DataMember]
        public object HuurPrijsTot { get; set; }

        [DataMember]
        public object Huurprijs { get; set; }

        [DataMember]
        public object HuurprijsFormaat { get; set; }

        [DataMember]
        public Guid Id { get; set; }

        [DataMember]
        public object InUnitsVanaf { get; set; }

        [DataMember]
        public bool IndProjectObjectType { get; set; }

        [DataMember]
        public object IndTransactieMakelaarTonen { get; set; }

        [DataMember]
        public bool IsSearchable { get; set; }

        [DataMember]
        public bool IsVerhuurd { get; set; }

        [DataMember]
        public bool IsVerkocht { get; set; }

        [DataMember]
        public bool IsVerkochtOfVerhuurd { get; set; }

        [DataMember]
        public long? Koopprijs { get; set; }

        [DataMember]
        public string KoopprijsFormaat { get; set; }

        [DataMember]
        public long? KoopprijsTot { get; set; }

        [DataMember]
        public long MakelaarId { get; set; }

        [DataMember]
        public string MakelaarNaam { get; set; }

        [DataMember]
        public Uri MobileUrl { get; set; }

        [DataMember]
        public object Note { get; set; }

        [DataMember]
        public object[] OpenHuis { get; set; }

        [DataMember]
        public long Oppervlakte { get; set; }

        [DataMember]
        public long? Perceeloppervlakte { get; set; }

        [DataMember]
        public string Postcode { get; set; }

        [DataMember]
        public Prijs Prijs { get; set; }

        [DataMember]
        public string PrijsGeformatteerdHtml { get; set; }

        [DataMember]
        public string PrijsGeformatteerdTextHuur { get; set; }

        [DataMember]
        public string PrijsGeformatteerdTextKoop { get; set; }

        [DataMember]
        public List<string> Producten { get; set; }

        [DataMember]
        public Project Project { get; set; }

        [DataMember]
        public object ProjectNaam { get; set; }

        [DataMember]
        public PromoLabel PromoLabel { get; set; }

        [DataMember]
        public string PublicatieDatum { get; set; }

        [DataMember]
        public long PublicatieStatus { get; set; }

        [DataMember]
        public object SavedDate { get; set; }

        [DataMember]
        public Object SoortAanbod { get; set; }

        [DataMember]
        public long ObjectSoortAanbod { get; set; }

        [DataMember]
        public object StartOplevering { get; set; }

        [DataMember]
        public object TimeAgoText { get; set; }

        [DataMember]
        public object TransactieAfmeldDatum { get; set; }

        [DataMember]
        public object TransactieMakelaarId { get; set; }

        [DataMember]
        public object TransactieMakelaarNaam { get; set; }

        [DataMember]
        public long TypeProject { get; set; }

        [DataMember]
        public Uri Url { get; set; }

        [DataMember]
        public Object VerkoopStatus { get; set; }

        [DataMember]
        public double Wgs84X { get; set; }

        [DataMember]
        public double Wgs84Y { get; set; }

        [DataMember]
        public long WoonOppervlakteTot { get; set; }

        [DataMember]
        public long Woonoppervlakte { get; set; }

        [DataMember]
        public Object Woonplaats { get; set; }

        [DataMember]
        public long[] ZoekType { get; set; }
    }

    public class Prijs
    {
        [DataMember]
        public bool GeenExtraKosten { get; set; }

        [DataMember]
        public string HuurAbbreviation { get; set; }

        [DataMember]
        public object Huurprijs { get; set; }

        [DataMember]
        public string HuurprijsOpAanvraag { get; set; }

        [DataMember]
        public object HuurprijsTot { get; set; }

        [DataMember]
        public string KoopAbbreviation { get; set; }

        [DataMember]
        public long? Koopprijs { get; set; }

        [DataMember]
        public string KoopprijsOpAanvraag { get; set; }

        [DataMember]
        public long? KoopprijsTot { get; set; }

        [DataMember]
        public object OriginelePrijs { get; set; }

        [DataMember]
        public string VeilingText { get; set; }
    }

    public class Project
    {
        [DataMember]
        public object AantalKamersTotEnMet { get; set; }

        [DataMember]
        public object AantalKamersVan { get; set; }

        [DataMember]
        public object AantalKavels { get; set; }

        [DataMember]
        public object Adres { get; set; }

        [DataMember]
        public object FriendlyUrl { get; set; }

        [DataMember]
        public object GewijzigdDatum { get; set; }

        [DataMember]
        public object GlobalId { get; set; }

        [DataMember]
        public string HoofdFoto { get; set; }

        [DataMember]
        public bool IndIpix { get; set; }

        [DataMember]
        public bool IndPdf { get; set; }

        [DataMember]
        public bool IndPlattegrond { get; set; }

        [DataMember]
        public bool IndTop { get; set; }

        [DataMember]
        public bool IndVideo { get; set; }

        [DataMember]
        public Guid InternalId { get; set; }

        [DataMember]
        public object MaxWoonoppervlakte { get; set; }

        [DataMember]
        public object MinWoonoppervlakte { get; set; }

        [DataMember]
        public object Naam { get; set; }

        [DataMember]
        public object Omschrijving { get; set; }

        [DataMember]
        public object[] OpenHuizen { get; set; }

        [DataMember]
        public object Plaats { get; set; }

        [DataMember]
        public object Prijs { get; set; }

        [DataMember]
        public object PrijsGeformatteerd { get; set; }

        [DataMember]
        public object PublicatieDatum { get; set; }

        [DataMember]
        public long Type { get; set; }

        [DataMember]
        public object Woningtypen { get; set; }
    }

    public class PromoLabel
    {
        [DataMember]
        public bool HasPromotionLabel { get; set; }

        [DataMember]
        public object[] PromotionPhotos { get; set; }

        [DataMember]
        public object PromotionPhotosSecure { get; set; }

        [DataMember]
        public long PromotionType { get; set; }

        [DataMember]
        public long RibbonColor { get; set; }

        [DataMember]
        public string RibbonText { get; set; }

        [DataMember]
        public string Tagline { get; set; }
    }

    [DataContract]
    public class Paging
    {
        [DataMember(Name = "AantalPaginas")]
        public int? AantalPaginas { get; set; }

        [DataMember(Name = "HuidigePagina")]
        public int? HuidigePagina { get; set; }

        [DataMember(Name = "VolgendeUrl")]
        public object VolgendeUrl { get; set; }

        [DataMember(Name = "VorigeUrl")]
        public object VorigeUrl { get; set; }
    }
}