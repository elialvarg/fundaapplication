﻿using System;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using BusinessLogicLayer.Interfaces;
using BusinessLogicLayer.Models;
using Common;
using Newtonsoft.Json;

namespace BusinessLogicLayer
{
    /// <summary>
    /// The aim of this Proxy is to access the resources in funda API.
    /// </summary>
    internal class FundaApiProxy : IFundaApiProxy
    {
        private readonly ILoggerWrapper _logger;

        public FundaApiProxy(ILoggerWrapper logger)
        {
            if (logger == null)
            {
                throw new ArgumentNullException($"Argument null in the call to FundaApiProxy.");
            }

            _logger = logger;
        }

        /// <summary>
        /// Gets all objects for sale from the url.
        /// </summary>
        /// <returns>FundaDto that contains the objects for sale.</returns>
        public async Task<FundaDto> GetFundaObjectDto(string url)
        {
            _logger.Info($"Calling FundaApi with url = {url}.");

            using (var client = new HttpClient())
            using (var request = new HttpRequestMessage(HttpMethod.Get, url))
            using (var response = await client.SendAsync(request))
            {
                var content = await response.Content.ReadAsStringAsync();

                if (response.IsSuccessStatusCode == false)
                {
                    _logger.Error($"An exception occured while calling FundaApi. Status = {(int)response.StatusCode}.");
                    throw new ApiException
                    {
                        StatusCode = (int)response.StatusCode,
                        Content = content
                    };
                }
                
                return JsonConvert.DeserializeObject<FundaDto>(content);
            }
        }
    }

    /// <summary>
    /// Api custom exception.
    /// </summary>
    public class ApiException : Exception
    {
        public int StatusCode { get; set; }

        public string Content { get; set; }
    }
}