﻿using System;
using System.Collections.Generic;
using System.Linq;
using BusinessLogicLayer.Interfaces;
using BusinessLogicLayer.Models;
using Common;
using DataAccessLayer.Interfaces;

namespace BusinessLogicLayer
{
    internal class AppLogic : IAppLogic
    {
        private const string UrlMakelaarAmsterdam = "http://partnerapi.funda.nl/feeds/Aanbod.svc/JSON/ac1b0b1572524640a0ecc54de453ea9f/?type=koop&zo=/amsterdam";
        private const string UrlMakelaarAmsterdamWithTuin = "http://partnerapi.funda.nl/feeds/Aanbod.svc/JSON/ac1b0b1572524640a0ecc54de453ea9f/?type=koop&zo=/amsterdam/tuin";

        private IFundaEntityRepository _fundaObjectRepository;
        private readonly IConsole _consoleWrapper;
        private readonly IFundaApiProxy _fundaApiProxy;
        private readonly IFundaDtoToEntityConverter _fundaDtoToEntityConverter;
        private readonly ILoggerWrapper _logger;

        public AppLogic(
            IFundaEntityRepository fundaObjectRepository, 
            IFundaApiProxy fundaApiProxy, 
            IConsole consoleWrapper, 
            IFundaDtoToEntityConverter fundaDtoToEntityConverter,
            ILoggerWrapper logger)
        {
            if (fundaObjectRepository == null || consoleWrapper == null || fundaApiProxy == null || fundaDtoToEntityConverter == null || logger == null)
            {
                throw new ArgumentNullException($"Argument null in the call to AppLogic.");
            }

            _fundaObjectRepository = fundaObjectRepository;
            _fundaApiProxy = fundaApiProxy;
            _consoleWrapper = consoleWrapper;
            _fundaDtoToEntityConverter = fundaDtoToEntityConverter;
            _logger = logger;
        }
        
        public void Run()
        {
            _consoleWrapper.WriteLine("**********************************************************************");
            _consoleWrapper.WriteLine("");
            _consoleWrapper.WriteLine("    Welcome to FundaApplication. ");
            _consoleWrapper.WriteLine("");
            _consoleWrapper.WriteLine("    Please type the word 'exit' when you want to stop the application.");
            _consoleWrapper.WriteLine("");
            _consoleWrapper.WriteLine("    * Type 1 for showing Top 10 makelaar's Amsterdam with most sale objects. *");
            _consoleWrapper.WriteLine("    * Type 2 for showing Top 10 makelaar's Amsterdam with most sale objects with a tuin. *");
            _consoleWrapper.WriteLine("");
            _consoleWrapper.WriteLine("**********************************************************************");
            _consoleWrapper.WriteLine("");

            while (true) // Loop indefinitely
            {
                string userInput = _consoleWrapper.ReadLine();

                if (userInput == "1")
                {
                    _logger.Info($"User type option 1.");
                    var TopMarkelaars =  GetMakelaars(_fundaObjectRepository.GetTopAmsterdamMakelaars, UrlMakelaarAmsterdam, 10).ToList();
                    TopMarkelaars.ForEach(i => _consoleWrapper.WriteLine(i.MakelaarName));
                    _consoleWrapper.WriteLine("");
                }
                else if (userInput == "2")
                {
                    _logger.Info($"User type option 2.");
                    var TopMarkelaarsWithTuin = GetMakelaars(_fundaObjectRepository.GetAmsterdamMakelaarsWithTuin, UrlMakelaarAmsterdamWithTuin, 10).ToList();
                    TopMarkelaarsWithTuin.ForEach(i => _consoleWrapper.WriteLine(i.MakelaarName));
                    _consoleWrapper.WriteLine("");
                }
                else if (userInput == "exit")
                {
                    _logger.Info($"Exit application.");
                    return;
                }
                else
                {
                    _logger.Info($"Unrecognized instruction. Instruction: {userInput}.");
                    _consoleWrapper.WriteLine("Unrecognized instruction. Please, type a new instruction.");
                    _consoleWrapper.WriteLine("");
                }
                
                _consoleWrapper.WriteLine("");
                _consoleWrapper.WriteLine("");
            }
        }
        
        private IList<FundaEntity> GetMakelaars(Func<int, IList<FundaEntity>> getTopAmsterdamMakelaars, string url, int numberOfMakelaars)
        {
            IList<FundaEntity> result = new List<FundaEntity>();
            
            // Clear the repository.
            _fundaObjectRepository.RemoveAll();

            var numberOfPages = GetTotalPage(url);
            if (numberOfPages.HasValue && numberOfPages > 0)
            {
                List<FundaObjectDto> fundaObjectsDtoList = new List<FundaObjectDto>();
                for (int i = 0; i < numberOfPages; i++)
                {
                    string urlWithPage = $"{url}/p{numberOfPages}";
                    try
                    {
                        FundaDto fundaDto = _fundaApiProxy.GetFundaObjectDto(urlWithPage).Result;
                        fundaObjectsDtoList.AddRange(fundaDto.Items);
                    }
                    catch (Exception e)
                    {
                        _logger.Error($"ApiException occured. Exception: {e.Message}.");
                    }
                }
                
                IList<FundaEntity> fundaEntities = _fundaDtoToEntityConverter.ToFundaEntity(fundaObjectsDtoList);
                _fundaObjectRepository.AddRange(fundaEntities);
                result = getTopAmsterdamMakelaars(numberOfMakelaars);
            }
            else
            {
                _logger.Error($"Error getting the number of pages in url = {url}.");
            }

            return result;
        }

        private int? GetTotalPage(string url)
        {
            int? result = null;

            try
            {
                var fundaObject = _fundaApiProxy.GetFundaObjectDto(url);
                result = fundaObject?.Result?.Paging?.AantalPaginas;
            }
            catch (ApiException e)
            {
                _logger.Error($"ApiException occured. Exception: {e.Message}.");
            }

            return result;
        }
    }
}