﻿using System;
using BusinessLogicLayer.Interfaces;

namespace BusinessLogicLayer
{
    /// <summary>
    /// Console wrapper created for testing purposes.
    /// </summary>
    internal class ConsoleWrapper : IConsole
    {
        public void WriteLine(string message)
        {
            Console.WriteLine(message);
        }

        public string ReadLine()
        {
            return Console.ReadLine();
        }
    }
}