﻿using System;
using System.Collections.Generic;
using System.Linq;
using BusinessLogicLayer.Interfaces;
using BusinessLogicLayer.Models;
using DataAccessLayer;
using DataAccessLayer.Interfaces;

namespace BusinessLogicLayer
{
    internal class FundaDtoToEntityConverter : IFundaDtoToEntityConverter
    {
        public IList<FundaEntity> ToFundaEntity(IList<FundaObjectDto> dto)
        {
            return dto.GroupBy(x => new { x.MakelaarNaam, x.MakelaarId })
                    .Select(x => new FundaEntityBuilder().WithMakelaarName(x.First().MakelaarNaam).WithTotalObjects(x.Count()).Build())
                    .OrderByDescending(t => t.TotalObjects).ToList();
        }
    }
}