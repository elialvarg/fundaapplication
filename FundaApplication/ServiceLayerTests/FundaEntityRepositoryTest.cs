﻿using System.Collections.Generic;
using System.Linq;
using DataAccessLayer;
using DataAccessLayer.Interfaces;
using NUnit.Framework;

namespace DataAccessLayerTests
{
    [TestFixture]
    public class FundaEntityRepositoryTest
    {
        private IFundaEntityRepository _fundaRepository;
        private List<FundaEntity> _context;

        [SetUp]
        public void SetUp()
        {
            _context = new List<FundaEntity>();
            _context = BuildListOfFundaObjects();
            _fundaRepository = new FundaEntityRepository(_context);
        }

        [TearDown]
        public void Cleanup()
        {
            _fundaRepository = null;
            _context = null;
        }

        [Test]
        public void GivenListOfFundaObjects_WhenCallingGetAmsterdamMakelaarsWithTuinWithParameterFive_ThenFiveItemsAreReturned()
        {
            // Arrange
            int numberOfItems = 5;
            var expectedList = _context.Take(numberOfItems);

            // Act
            var result = _fundaRepository.GetAmsterdamMakelaarsWithTuin(numberOfItems);

            // Assert
            CollectionAssert.AreEqual(expectedList.ToList(), result.ToList(), Comparer());
        }

        [Test]
        public void GivenListOfFundaObjects_WhenCallingGetAmsterdamMakelaarsWithTuinWithParameterSeven_ThenSevenItemsAreReturned()
        {
            // Arrange
            int numberOfItems = 7;
            var expectedList = _context.Take(numberOfItems);

            // Act
            var result = _fundaRepository.GetTopAmsterdamMakelaars(numberOfItems);

            // Assert
            CollectionAssert.AreEqual(expectedList.ToList(), result.ToList(), Comparer());
        }

        private static Comparer<FundaEntity> Comparer()
        {
            return Comparer<FundaEntity>.Create((x, y) => x.MakelaarName == y.MakelaarName ? 0 : -1);
        }

        private List<FundaEntity> BuildListOfFundaObjects()
        {
            FundaEntity p0 = new FundaEntityBuilder().WithMakelaarName("Makelaar1").WithTotalObjects(76).Build();
            FundaEntity p1 = new FundaEntityBuilder().WithMakelaarName("Makelaar2").WithTotalObjects(6).Build();
            FundaEntity p2 = new FundaEntityBuilder().WithMakelaarName("Makelaar3").WithTotalObjects(8).Build();
            FundaEntity p3 = new FundaEntityBuilder().WithMakelaarName("Makelaar4").WithTotalObjects(56).Build();
            FundaEntity p4 = new FundaEntityBuilder().WithMakelaarName("Makelaar5").WithTotalObjects(7).Build(); 
            FundaEntity p5 = new FundaEntityBuilder().WithMakelaarName("Makelaar6").WithTotalObjects(756).Build();
            FundaEntity p6 = new FundaEntityBuilder().WithMakelaarName("Makelaar7").WithTotalObjects(7).Build();
            FundaEntity p7 = new FundaEntityBuilder().WithMakelaarName("Makelaar8").WithTotalObjects(65).Build();
            FundaEntity p8 = new FundaEntityBuilder().WithMakelaarName("Makelaar9").WithTotalObjects(9).Build();
            FundaEntity p9 = new FundaEntityBuilder().WithMakelaarName("Makelaar10").WithTotalObjects(1).Build();

            return new List<FundaEntity> { p0, p1, p2, p3, p4, p5, p6, p7, p8, p9 };
        }
    }
}