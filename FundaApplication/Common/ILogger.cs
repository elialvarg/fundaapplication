﻿namespace Common
{
    public interface ILoggerWrapper
    {
        void Debug(object message);

        void Error(object message);

        void Info(object message);

        bool IsDebugEnabled { get; }
    }
}