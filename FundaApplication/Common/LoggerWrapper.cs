﻿namespace Common
{
    /// <summary>
    /// Wrapper of the Log4Net. This is placed in the common layer because is used by the rest of layers.
    /// </summary>
    public class Log4NetWrapper : ILoggerWrapper
    {
        private readonly log4net.ILog _logger;

        public Log4NetWrapper()
        {
            _logger = log4net.LogManager.GetLogger("FundaApplicationLogger");
        }
        public void Debug(object message)
        {
            _logger.Debug(message);
        }

        public void Error(object message)
        {
            _logger.Error(message);
        }

        public void Info(object message)
        {
            _logger.Info(message);
        }

        public bool IsDebugEnabled
        {
            get { return _logger.IsDebugEnabled; }
        }
    }
}
