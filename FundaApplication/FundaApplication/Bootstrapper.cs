﻿using System.Collections.Generic;
using BusinessLogicLayer;
using BusinessLogicLayer.Interfaces;
using Common;
using DataAccessLayer;
using DataAccessLayer.Interfaces;
using Unity;
using Unity.Injection;
using Unity.Lifetime;

namespace ApplicationLayer
{
    /// <summary>
    /// The aim of this class is to register all the necessary dependencies of the application and use Dependency Injection.
    /// </summary>
    public static class Bootstrapper
    {
        public static UnityContainer Container;

        public static void Start()
        {
            Container = new UnityContainer();
            RegisterTypes();
        }

        public static void RegisterTypes()
        {
            Container.RegisterType<ILoggerWrapper, Log4NetWrapper>(new ContainerControlledLifetimeManager());
            Container.RegisterType<IAppLogic, AppLogic>();
            Container.RegisterType<IFundaApiProxy, FundaApiProxy>();
            Container.RegisterType<IFundaEntityRepository, FundaEntityRepository>();
            Container.RegisterType<IConsole, ConsoleWrapper>();
            Container.RegisterType<IFundaDtoToEntityConverter, FundaDtoToEntityConverter>();
            Container.RegisterType<IFundaEntityBuilder, FundaEntityBuilder>();
            Container.RegisterType<IFundaEntityRepository, FundaEntityRepository>(new InjectionConstructor(new List<FundaEntity>()));
        }

        public static void Stop()
        {
            Container.Dispose();
        }
    }
}