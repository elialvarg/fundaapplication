﻿using BusinessLogicLayer;
using Unity;

namespace ApplicationLayer
{
    public class Program
    {
        static void Main(string[] args)
        {
            Bootstrapper.Start();
            
            IAppLogic appLogic = Bootstrapper.Container.Resolve<IAppLogic>();
            appLogic.Run();

            Bootstrapper.Stop();
        }
    }
}