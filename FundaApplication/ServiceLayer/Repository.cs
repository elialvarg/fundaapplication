﻿using System;
using System.Collections.Generic;
using System.Linq;
using DataAccessLayer.Interfaces;
using DataAccessLayer.Models;

namespace DataAccessLayer
{
    internal class Repository<TEntity> : IRepository<TEntity> where TEntity : EntityBase
    {
        private List<TEntity> _context;

        public Repository(List<TEntity> context)
        {
            _context = context;
        }

        public TEntity Get(Guid id)
        {
            return _context.SingleOrDefault(x => x.Id == id);
        }

        public IList<TEntity> GetAll()
        {
            return _context;
        }

        public void Add(TEntity entity)
        {
            _context.Add(entity);
        }
        
        public void AddRange(IList<TEntity> entities)
        {
            _context.AddRange(entities);
        }
        
        public IList<TEntity> Take(int numberOfEntities)
        {
            return _context.Take(numberOfEntities).ToList();
        }
        
        public void Remove(TEntity entity)
        {
            _context.Remove(entity);
        }

        public void RemoveAll()
        {
            _context.Clear();
        }
    }
}