﻿using System;
using DataAccessLayer.Interfaces;

namespace DataAccessLayer
{
    /// <summary>
    /// Constructs the entity FundaEntity.
    /// </summary>
    public class FundaEntityBuilder : IFundaEntityBuilder
    {
        private readonly FundaEntity _fundaEntity;

        public FundaEntityBuilder()
        {
            _fundaEntity = new FundaEntity { Id = Guid.NewGuid() };
        }

        public FundaEntityBuilder WithMakelaarName(string makelaarName)
        {
            _fundaEntity.MakelaarName = makelaarName;
            return this;
        }

        public FundaEntityBuilder WithTotalObjects(int totalObjects)
        {
            _fundaEntity.TotalObjects = totalObjects;
            return this;
        }
        
        public FundaEntity Build()
        {
            return _fundaEntity;
        }
    }
}