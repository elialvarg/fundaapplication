﻿using System.Collections.Generic;
using System.Linq;
using DataAccessLayer.Interfaces;

namespace DataAccessLayer
{
    /// <summary>
    /// The repository that manages the list of objects for sale.
    /// </summary>
    internal class FundaEntityRepository : Repository<FundaEntity>, IFundaEntityRepository
    {
        public FundaEntityRepository(List<FundaEntity> context) : base(context)
        {

        }
        
        public List<FundaEntity> GetTopAmsterdamMakelaars(int numberOfMakelaars)
        {
            return base.Take(numberOfMakelaars).ToList();
        }

        public List<FundaEntity> GetAmsterdamMakelaarsWithTuin(int numberOfMakelaars)
        {
            return base.Take(numberOfMakelaars).ToList();
        }
    }
}