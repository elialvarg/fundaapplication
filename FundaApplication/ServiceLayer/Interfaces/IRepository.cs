﻿using System;
using System.Collections.Generic;
using System.Linq;
using DataAccessLayer.Models;

namespace DataAccessLayer.Interfaces
{
    public interface IRepository<TEntity> where TEntity : EntityBase
    {
        TEntity Get(Guid id);

        IList<TEntity> GetAll();

        void Add(TEntity entity);

        void AddRange(IList<TEntity> entities);

        void Remove(TEntity entity);

        IList<TEntity> Take(int numberOfEntities);
        
        void RemoveAll();
    }
}