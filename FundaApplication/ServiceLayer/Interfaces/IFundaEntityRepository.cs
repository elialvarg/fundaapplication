﻿using System.Collections.Generic;

namespace DataAccessLayer.Interfaces
{
    /// <summary>
    /// The repository that manages the list of objects for sale.
    /// </summary>
    public interface IFundaEntityRepository : IRepository<FundaEntity>
    {
        List<FundaEntity> GetTopAmsterdamMakelaars(int numberOfMakelaars);

        List<FundaEntity> GetAmsterdamMakelaarsWithTuin(int numberOfMakelaars);
    }
}