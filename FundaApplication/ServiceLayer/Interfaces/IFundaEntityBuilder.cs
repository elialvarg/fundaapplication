﻿namespace DataAccessLayer.Interfaces
{
    /// <summary>
    /// Constructs the entity FundaEntity.
    /// </summary>
    public interface IFundaEntityBuilder
    {
        FundaEntityBuilder WithMakelaarName(string makelaarName);

        FundaEntityBuilder WithTotalObjects(int totalObjects);
        
        FundaEntity Build();
    }
}