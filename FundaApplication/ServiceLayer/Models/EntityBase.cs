﻿using System;

namespace DataAccessLayer.Models
{
    public abstract class EntityBase
    {
        public Guid Id { get; set; }
    }
}