﻿using DataAccessLayer.Models;

namespace DataAccessLayer.Interfaces
{
    public class FundaEntity : EntityBase
    {
        public string MakelaarName { get; set; }

        public int TotalObjects { get; set; }
    }
}