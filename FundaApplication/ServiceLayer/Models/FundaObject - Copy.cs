﻿using System.Collections.Generic;

namespace DataAccessLayer.DomainObjects
{
    using System;
    using System.Globalization;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;

    public partial class FundaObject
    {
        /*[JsonProperty("AccountStatus")]
        public long AccountStatus { get; set; }

        [JsonProperty("EmailNotConfirmed")]
        public bool EmailNotConfirmed { get; set; }

        [JsonProperty("ValidationFailed")]
        public bool ValidationFailed { get; set; }

        [JsonProperty("ValidationReport")]
        public object ValidationReport { get; set; }

        [JsonProperty("Website")]
        public long Website { get; set; }

        [JsonProperty("Metadata")]
        //public Metadata Metadata { get; set; }
        public Metadata Metadata { get; set; }*/

        [JsonProperty("Objects")]
        public List<Item> Items { get; set; }

        /*[JsonProperty("Paging")]
        //public Paging Paging { get; set; }
        public Paging Paging { get; set; }

        [JsonProperty("TotaalAantalObjecten")]
        public long TotaalAantalObjecten { get; set; }*/
    }

    public partial class Metadata
    {
        [JsonProperty("ObjectType")]
        public string ObjectType { get; set; }

        [JsonProperty("Omschrijving")]
        public string Omschrijving { get; set; }

        [JsonProperty("Titel")]
        public string Titel { get; set; }
    }

    [JsonArray("Objects")]
    public partial class Item
    {
        [JsonProperty("AangebodenSindsTekst")]
        public string AangebodenSindsTekst { get; set; }

        [JsonProperty("AanmeldDatum")]
        public string AanmeldDatum { get; set; }

        [JsonProperty("AantalBeschikbaar")]
        public object AantalBeschikbaar { get; set; }

        [JsonProperty("AantalKamers")]
        public long AantalKamers { get; set; }

        [JsonProperty("AantalKavels")]
        public object AantalKavels { get; set; }

        [JsonProperty("Aanvaarding")]
        public string Aanvaarding { get; set; }

        [JsonProperty("Adres")]
        public string Adres { get; set; }

        [JsonProperty("Afstand")]
        public long Afstand { get; set; }

        [JsonProperty("BronCode")]
        public string BronCode { get; set; }

        [JsonProperty("ChildrenObjects")]
        public object[] ChildrenObjects { get; set; }

        [JsonProperty("DatumAanvaarding")]
        public object DatumAanvaarding { get; set; }

        [JsonProperty("DatumOndertekeningAkte")]
        public object DatumOndertekeningAkte { get; set; }

        [JsonProperty("Foto")]
        public Uri Foto { get; set; }

        [JsonProperty("FotoLarge")]
        public Uri FotoLarge { get; set; }

        [JsonProperty("FotoLargest")]
        public Uri FotoLargest { get; set; }

        [JsonProperty("FotoMedium")]
        public Uri FotoMedium { get; set; }

        [JsonProperty("FotoSecure")]
        public Uri FotoSecure { get; set; }

        [JsonProperty("GewijzigdDatum")]
        public object GewijzigdDatum { get; set; }

        [JsonProperty("GlobalId")]
        public long GlobalId { get; set; }

        [JsonProperty("GroupByObjectType")]
        public Guid GroupByObjectType { get; set; }

        [JsonProperty("Heeft360GradenFoto")]
        public bool Heeft360GradenFoto { get; set; }

        [JsonProperty("HeeftBrochure")]
        public bool HeeftBrochure { get; set; }

        [JsonProperty("HeeftOpenhuizenTopper")]
        public bool HeeftOpenhuizenTopper { get; set; }

        [JsonProperty("HeeftOverbruggingsgrarantie")]
        public bool HeeftOverbruggingsgrarantie { get; set; }

        [JsonProperty("HeeftPlattegrond")]
        public bool HeeftPlattegrond { get; set; }

        [JsonProperty("HeeftTophuis")]
        public bool HeeftTophuis { get; set; }

        [JsonProperty("HeeftVeiling")]
        public bool HeeftVeiling { get; set; }

        [JsonProperty("HeeftVideo")]
        public bool HeeftVideo { get; set; }

        [JsonProperty("HuurPrijsTot")]
        public object HuurPrijsTot { get; set; }

        [JsonProperty("Huurprijs")]
        public object Huurprijs { get; set; }

        [JsonProperty("HuurprijsFormaat")]
        public object HuurprijsFormaat { get; set; }

        [JsonProperty("Id")]
        public Guid Id { get; set; }

        [JsonProperty("InUnitsVanaf")]
        public object InUnitsVanaf { get; set; }

        [JsonProperty("IndProjectObjectType")]
        public bool IndProjectObjectType { get; set; }

        [JsonProperty("IndTransactieMakelaarTonen")]
        public object IndTransactieMakelaarTonen { get; set; }

        [JsonProperty("IsSearchable")]
        public bool IsSearchable { get; set; }

        [JsonProperty("IsVerhuurd")]
        public bool IsVerhuurd { get; set; }

        [JsonProperty("IsVerkocht")]
        public bool IsVerkocht { get; set; }

        [JsonProperty("IsVerkochtOfVerhuurd")]
        public bool IsVerkochtOfVerhuurd { get; set; }

        [JsonProperty("Koopprijs")]
        public long Koopprijs { get; set; }

        [JsonProperty("KoopprijsFormaat")]
        public string KoopprijsFormaat { get; set; }

        [JsonProperty("KoopprijsTot")]
        public long KoopprijsTot { get; set; }

        [JsonProperty("MakelaarId")]
        public long MakelaarId { get; set; }

        [JsonProperty("MakelaarNaam")]
        public string MakelaarNaam { get; set; }

        [JsonProperty("MobileURL")]
        public Uri MobileUrl { get; set; }

        [JsonProperty("Note")]
        public object Note { get; set; }

        [JsonProperty("OpenHuis")]
        public object[] OpenHuis { get; set; }

        [JsonProperty("Oppervlakte")]
        public long Oppervlakte { get; set; }

        [JsonProperty("Perceeloppervlakte")]
        public long? Perceeloppervlakte { get; set; }

        [JsonProperty("Postcode")]
        public string Postcode { get; set; }

        [JsonProperty("Prijs")]
        public Prijs Prijs { get; set; }

        [JsonProperty("PrijsGeformatteerdHtml")]
        public string PrijsGeformatteerdHtml { get; set; }

        [JsonProperty("PrijsGeformatteerdTextHuur")]
        public string PrijsGeformatteerdTextHuur { get; set; }

        [JsonProperty("PrijsGeformatteerdTextKoop")]
        public string PrijsGeformatteerdTextKoop { get; set; }

        [JsonProperty("Producten")]
        public List<string> Producten { get; set; }

        [JsonProperty("Project")]
        public Project Project { get; set; }

        [JsonProperty("ProjectNaam")]
        public object ProjectNaam { get; set; }

        [JsonProperty("PromoLabel")]
        public PromoLabel PromoLabel { get; set; }

        [JsonProperty("PublicatieDatum")]
        public string PublicatieDatum { get; set; }

        [JsonProperty("PublicatieStatus")]
        public long PublicatieStatus { get; set; }

        [JsonProperty("SavedDate")]
        public object SavedDate { get; set; }

        [JsonProperty("Soort-aanbod")]
        public SoortAanbod SoortAanbod { get; set; }

        [JsonProperty("SoortAanbod")]
        public long ObjectSoortAanbod { get; set; }

        [JsonProperty("StartOplevering")]
        public object StartOplevering { get; set; }

        [JsonProperty("TimeAgoText")]
        public object TimeAgoText { get; set; }

        [JsonProperty("TransactieAfmeldDatum")]
        public object TransactieAfmeldDatum { get; set; }

        [JsonProperty("TransactieMakelaarId")]
        public object TransactieMakelaarId { get; set; }

        [JsonProperty("TransactieMakelaarNaam")]
        public object TransactieMakelaarNaam { get; set; }

        [JsonProperty("TypeProject")]
        public long TypeProject { get; set; }

        [JsonProperty("URL")]
        public Uri Url { get; set; }

        [JsonProperty("VerkoopStatus")]
        public VerkoopStatus VerkoopStatus { get; set; }

        [JsonProperty("WGS84_X")]
        public double Wgs84X { get; set; }

        [JsonProperty("WGS84_Y")]
        public double Wgs84Y { get; set; }

        [JsonProperty("WoonOppervlakteTot")]
        public long WoonOppervlakteTot { get; set; }

        [JsonProperty("Woonoppervlakte")]
        public long Woonoppervlakte { get; set; }

        [JsonProperty("Woonplaats")]
        public Woonplaats Woonplaats { get; set; }

        [JsonProperty("ZoekType")]
        public long[] ZoekType { get; set; }
    }

    public partial class Prijs
    {
        [JsonProperty("GeenExtraKosten")]
        public bool GeenExtraKosten { get; set; }

        [JsonProperty("HuurAbbreviation")]
        public string HuurAbbreviation { get; set; }

        [JsonProperty("Huurprijs")]
        public object Huurprijs { get; set; }

        [JsonProperty("HuurprijsOpAanvraag")]
        public string HuurprijsOpAanvraag { get; set; }

        [JsonProperty("HuurprijsTot")]
        public object HuurprijsTot { get; set; }

        [JsonProperty("KoopAbbreviation")]
        public string KoopAbbreviation { get; set; }

        [JsonProperty("Koopprijs")]
        public long Koopprijs { get; set; }

        [JsonProperty("KoopprijsOpAanvraag")]
        public string KoopprijsOpAanvraag { get; set; }

        [JsonProperty("KoopprijsTot")]
        public long KoopprijsTot { get; set; }

        [JsonProperty("OriginelePrijs")]
        public object OriginelePrijs { get; set; }

        [JsonProperty("VeilingText")]
        public string VeilingText { get; set; }
    }

    public partial class Project
    {
        [JsonProperty("AantalKamersTotEnMet")]
        public object AantalKamersTotEnMet { get; set; }

        [JsonProperty("AantalKamersVan")]
        public object AantalKamersVan { get; set; }

        [JsonProperty("AantalKavels")]
        public object AantalKavels { get; set; }

        [JsonProperty("Adres")]
        public object Adres { get; set; }

        [JsonProperty("FriendlyUrl")]
        public object FriendlyUrl { get; set; }

        [JsonProperty("GewijzigdDatum")]
        public object GewijzigdDatum { get; set; }

        [JsonProperty("GlobalId")]
        public object GlobalId { get; set; }

        [JsonProperty("HoofdFoto")]
        public IList<string> HoofdFoto { get; set; }

        [JsonProperty("IndIpix")]
        public bool IndIpix { get; set; }

        [JsonProperty("IndPDF")]
        public bool IndPdf { get; set; }

        [JsonProperty("IndPlattegrond")]
        public bool IndPlattegrond { get; set; }

        [JsonProperty("IndTop")]
        public bool IndTop { get; set; }

        [JsonProperty("IndVideo")]
        public bool IndVideo { get; set; }

        [JsonProperty("InternalId")]
        public Guid InternalId { get; set; }

        [JsonProperty("MaxWoonoppervlakte")]
        public object MaxWoonoppervlakte { get; set; }

        [JsonProperty("MinWoonoppervlakte")]
        public object MinWoonoppervlakte { get; set; }

        [JsonProperty("Naam")]
        public object Naam { get; set; }

        [JsonProperty("Omschrijving")]
        public object Omschrijving { get; set; }

        [JsonProperty("OpenHuizen")]
        public object[] OpenHuizen { get; set; }

        [JsonProperty("Plaats")]
        public object Plaats { get; set; }

        [JsonProperty("Prijs")]
        public object Prijs { get; set; }

        [JsonProperty("PrijsGeformatteerd")]
        public object PrijsGeformatteerd { get; set; }

        [JsonProperty("PublicatieDatum")]
        public object PublicatieDatum { get; set; }

        [JsonProperty("Type")]
        public long Type { get; set; }

        [JsonProperty("Woningtypen")]
        public object Woningtypen { get; set; }
    }

    public partial class PromoLabel
    {
        [JsonProperty("HasPromotionLabel")]
        public bool HasPromotionLabel { get; set; }

        [JsonProperty("PromotionPhotos")]
        public object[] PromotionPhotos { get; set; }

        [JsonProperty("PromotionPhotosSecure")]
        public object PromotionPhotosSecure { get; set; }

        [JsonProperty("PromotionType")]
        public long PromotionType { get; set; }

        [JsonProperty("RibbonColor")]
        public long RibbonColor { get; set; }

        [JsonProperty("RibbonText")]
        public string RibbonText { get; set; }

        [JsonProperty("Tagline")]
        public string Tagline { get; set; }
    }

    public partial class Paging
    {
        [JsonProperty("AantalPaginas")]
        public long AantalPaginas { get; set; }

        [JsonProperty("HuidigePagina")]
        public long HuidigePagina { get; set; }

        [JsonProperty("VolgendeUrl")]
        public string VolgendeUrl { get; set; }

        [JsonProperty("VorigeUrl")]
        public object VorigeUrl { get; set; }
    }

    public enum AanmeldDatum { Date12623004000000100 };

    public enum Aanvaarding { InOverleg };

    public enum BronCode { Nvm, Vbo };

    public enum KoopprijsFormaat { KoopPrijsKostenKoperKort };

    public enum KoopAbbreviation { KK };

    public enum HoofdFoto { ImgThumbsThumbGeenFotoGif };

    public enum SoortAanbod { Appartement, Woonhuis };

    public enum VerkoopStatus { StatusBeschikbaar };

    public enum Woonplaats { Amsterdam };

    public partial class FundaObject
    {
        public static FundaObject FromJson(string json) => JsonConvert.DeserializeObject<FundaObject>(json, Converter.Settings);
    }

    public static class Serialize
    {
        public static string ToJson(this FundaObject self) => JsonConvert.SerializeObject(self, Converter.Settings);
    }

    internal static class Converter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters = {
                AanmeldDatumConverter.Singleton,
                AanvaardingConverter.Singleton,
                BronCodeConverter.Singleton,
                KoopprijsFormaatConverter.Singleton,
                KoopAbbreviationConverter.Singleton,
                //ProductenConverter.Singleton,
                HoofdFotoConverter.Singleton,
                SoortAanbodConverter.Singleton,
                VerkoopStatusConverter.Singleton,
                WoonplaatsConverter.Singleton,
                new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
            },
        };
    }

    internal class AanmeldDatumConverter : JsonConverter
    {
        public override bool CanConvert(Type t) => t == typeof(AanmeldDatum) || t == typeof(AanmeldDatum?);

        public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
        {
            if (reader.TokenType == JsonToken.Null) return null;
            var value = serializer.Deserialize<string>(reader);
            if (value == "/Date(1262300400000+0100)/")
            {
                return AanmeldDatum.Date12623004000000100;
            }
            throw new Exception("Cannot unmarshal type AanmeldDatum");
        }

        public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
        {
            if (untypedValue == null)
            {
                serializer.Serialize(writer, null);
                return;
            }
            var value = (AanmeldDatum)untypedValue;
            if (value == AanmeldDatum.Date12623004000000100)
            {
                serializer.Serialize(writer, "/Date(1262300400000+0100)/");
                return;
            }
            throw new Exception("Cannot marshal type AanmeldDatum");
        }

        public static readonly AanmeldDatumConverter Singleton = new AanmeldDatumConverter();
    }

    internal class AanvaardingConverter : JsonConverter
    {
        public override bool CanConvert(Type t) => t == typeof(Aanvaarding) || t == typeof(Aanvaarding?);

        public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
        {
            if (reader.TokenType == JsonToken.Null) return null;
            var value = serializer.Deserialize<string>(reader);
            if (value == "InOverleg")
            {
                return Aanvaarding.InOverleg;
            }
            throw new Exception("Cannot unmarshal type Aanvaarding");
        }

        public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
        {
            if (untypedValue == null)
            {
                serializer.Serialize(writer, null);
                return;
            }
            var value = (Aanvaarding)untypedValue;
            if (value == Aanvaarding.InOverleg)
            {
                serializer.Serialize(writer, "InOverleg");
                return;
            }
            throw new Exception("Cannot marshal type Aanvaarding");
        }

        public static readonly AanvaardingConverter Singleton = new AanvaardingConverter();
    }

    internal class BronCodeConverter : JsonConverter
    {
        public override bool CanConvert(Type t) => t == typeof(BronCode) || t == typeof(BronCode?);

        public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
        {
            if (reader.TokenType == JsonToken.Null) return null;
            var value = serializer.Deserialize<string>(reader);
            switch (value)
            {
                case "NVM":
                    return BronCode.Nvm;
                case "VBO":
                    return BronCode.Vbo;
            }
            throw new Exception("Cannot unmarshal type BronCode");
        }

        public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
        {
            if (untypedValue == null)
            {
                serializer.Serialize(writer, null);
                return;
            }
            var value = (BronCode)untypedValue;
            switch (value)
            {
                case BronCode.Nvm:
                    serializer.Serialize(writer, "NVM");
                    return;
                case BronCode.Vbo:
                    serializer.Serialize(writer, "VBO");
                    return;
            }
            throw new Exception("Cannot marshal type BronCode");
        }

        public static readonly BronCodeConverter Singleton = new BronCodeConverter();
    }

    internal class KoopprijsFormaatConverter : JsonConverter
    {
        public override bool CanConvert(Type t) => t == typeof(KoopprijsFormaat) || t == typeof(KoopprijsFormaat?);

        public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
        {
            if (reader.TokenType == JsonToken.Null) return null;
            var value = serializer.Deserialize<string>(reader);
            if (value == "<[KoopPrijs]> <{kosten koper|kort}>")
            {
                return KoopprijsFormaat.KoopPrijsKostenKoperKort;
            }
            throw new Exception("Cannot unmarshal type KoopprijsFormaat");
        }

        public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
        {
            if (untypedValue == null)
            {
                serializer.Serialize(writer, null);
                return;
            }
            var value = (KoopprijsFormaat)untypedValue;
            if (value == KoopprijsFormaat.KoopPrijsKostenKoperKort)
            {
                serializer.Serialize(writer, "<[KoopPrijs]> <{kosten koper|kort}>");
                return;
            }
            throw new Exception("Cannot marshal type KoopprijsFormaat");
        }

        public static readonly KoopprijsFormaatConverter Singleton = new KoopprijsFormaatConverter();
    }

    internal class KoopAbbreviationConverter : JsonConverter
    {
        public override bool CanConvert(Type t) => t == typeof(KoopAbbreviation) || t == typeof(KoopAbbreviation?);

        public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
        {
            if (reader.TokenType == JsonToken.Null) return null;
            var value = serializer.Deserialize<string>(reader);
            if (value == "k.k.")
            {
                return KoopAbbreviation.KK;
            }
            throw new Exception("Cannot unmarshal type KoopAbbreviation");
        }

        public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
        {
            if (untypedValue == null)
            {
                serializer.Serialize(writer, null);
                return;
            }
            var value = (KoopAbbreviation)untypedValue;
            if (value == KoopAbbreviation.KK)
            {
                serializer.Serialize(writer, "k.k.");
                return;
            }
            throw new Exception("Cannot marshal type KoopAbbreviation");
        }

        public static readonly KoopAbbreviationConverter Singleton = new KoopAbbreviationConverter();
    }

    /*internal class ProductenConverter : JsonConverter
    {
        public override bool CanConvert(Type t) => t == typeof(Producten) || t == typeof(Producten?);

        public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
        {
            if (reader.TokenType == JsonToken.Null) return null;
            var value = serializer.Deserialize<string>(reader);
            switch (value)
            {
                case "360-fotos":
                    return Producten.The360Fotos;
                case "Brochure":
                    return Producten.Brochure;
                case "Featured":
                    return Producten.Featured;
                case "Plattegrond":
                    return Producten.Plattegrond;
                case "Video":
                    return Producten.Video;
            }
            throw new Exception("Cannot unmarshal type Producten");
        }

        public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
        {
            if (untypedValue == null)
            {
                serializer.Serialize(writer, null);
                return;
            }
            var value = (Producten)untypedValue;
            switch (value)
            {
                case Producten.The360Fotos:
                    serializer.Serialize(writer, "360-fotos");
                    return;
                case Producten.Brochure:
                    serializer.Serialize(writer, "Brochure");
                    return;
                case Producten.Featured:
                    serializer.Serialize(writer, "Featured");
                    return;
                case Producten.Plattegrond:
                    serializer.Serialize(writer, "Plattegrond");
                    return;
                case Producten.Video:
                    serializer.Serialize(writer, "Video");
                    return;
            }
            throw new Exception("Cannot marshal type Producten");
        }

        public static readonly ProductenConverter Singleton = new ProductenConverter();
    }*/

    internal class HoofdFotoConverter : JsonConverter
    {
        public override bool CanConvert(Type t) => t == typeof(HoofdFoto) || t == typeof(HoofdFoto?);

        public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
        {
            if (reader.TokenType == JsonToken.Null) return null;
            var value = serializer.Deserialize<string>(reader);
            if (value == "/img/thumbs/thumb-geen-foto.gif")
            {
                return HoofdFoto.ImgThumbsThumbGeenFotoGif;
            }
            throw new Exception("Cannot unmarshal type HoofdFoto");
        }

        public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
        {
            if (untypedValue == null)
            {
                serializer.Serialize(writer, null);
                return;
            }
            var value = (HoofdFoto)untypedValue;
            if (value == HoofdFoto.ImgThumbsThumbGeenFotoGif)
            {
                serializer.Serialize(writer, "/img/thumbs/thumb-geen-foto.gif");
                return;
            }
            throw new Exception("Cannot marshal type HoofdFoto");
        }

        public static readonly HoofdFotoConverter Singleton = new HoofdFotoConverter();
    }

    internal class SoortAanbodConverter : JsonConverter
    {
        public override bool CanConvert(Type t) => t == typeof(SoortAanbod) || t == typeof(SoortAanbod?);

        public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
        {
            if (reader.TokenType == JsonToken.Null) return null;
            var value = serializer.Deserialize<string>(reader);
            switch (value)
            {
                case "appartement":
                    return SoortAanbod.Appartement;
                case "woonhuis":
                    return SoortAanbod.Woonhuis;
            }
            throw new Exception("Cannot unmarshal type SoortAanbod");
        }

        public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
        {
            if (untypedValue == null)
            {
                serializer.Serialize(writer, null);
                return;
            }
            var value = (SoortAanbod)untypedValue;
            switch (value)
            {
                case SoortAanbod.Appartement:
                    serializer.Serialize(writer, "appartement");
                    return;
                case SoortAanbod.Woonhuis:
                    serializer.Serialize(writer, "woonhuis");
                    return;
            }
            throw new Exception("Cannot marshal type SoortAanbod");
        }

        public static readonly SoortAanbodConverter Singleton = new SoortAanbodConverter();
    }

    internal class VerkoopStatusConverter : JsonConverter
    {
        public override bool CanConvert(Type t) => t == typeof(VerkoopStatus) || t == typeof(VerkoopStatus?);

        public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
        {
            if (reader.TokenType == JsonToken.Null) return null;
            var value = serializer.Deserialize<string>(reader);
            if (value == "StatusBeschikbaar")
            {
                return VerkoopStatus.StatusBeschikbaar;
            }
            throw new Exception("Cannot unmarshal type VerkoopStatus");
        }

        public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
        {
            if (untypedValue == null)
            {
                serializer.Serialize(writer, null);
                return;
            }
            var value = (VerkoopStatus)untypedValue;
            if (value == VerkoopStatus.StatusBeschikbaar)
            {
                serializer.Serialize(writer, "StatusBeschikbaar");
                return;
            }
            throw new Exception("Cannot marshal type VerkoopStatus");
        }

        public static readonly VerkoopStatusConverter Singleton = new VerkoopStatusConverter();
    }

    internal class WoonplaatsConverter : JsonConverter
    {
        public override bool CanConvert(Type t) => t == typeof(Woonplaats) || t == typeof(Woonplaats?);

        public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
        {
            if (reader.TokenType == JsonToken.Null) return null;
            var value = serializer.Deserialize<string>(reader);
            if (value == "Amsterdam")
            {
                return Woonplaats.Amsterdam;
            }
            throw new Exception("Cannot unmarshal type Woonplaats");
        }

        public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
        {
            if (untypedValue == null)
            {
                serializer.Serialize(writer, null);
                return;
            }
            var value = (Woonplaats)untypedValue;
            if (value == Woonplaats.Amsterdam)
            {
                serializer.Serialize(writer, "Amsterdam");
                return;
            }
            throw new Exception("Cannot marshal type Woonplaats");
        }

        public static readonly WoonplaatsConverter Singleton = new WoonplaatsConverter();
    }
}